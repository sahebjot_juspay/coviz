## Requirements
 - SteamVR installed
 - SteamVR plugin installed from asset store
 - https://github.com/GlitchEnzo/NuGetForUnity for package management (Jurassic package), not necessary until new nuget packages to be installed

## For the js engine documentation
  - https://github.com/paulbartrum/jurassic/wiki/ various examples

## JsScripts
  - right now .txt files to avoid compilation errors in the js file
  - all the js files will be bundled togther to a bundle.js/.txt file
