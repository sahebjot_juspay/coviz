﻿using Jurassic;
using Jurassic.Library;
using UnityEngine;

public class JsBridge : ObjectInstance
{

    public JsBridge(ScriptEngine engine)
        : base(engine)
    {
        // searches for [JsFunction] and creates function
        this.PopulateFunctions();

        // examples of creating properties in js
        // Read-write property (name).
        //this["name"] = "prop in js";

        // Read-only property (version).
        //this.DefineProperty("version", new PropertyDescriptor(5, PropertyAttributes.Sealed), true);
    }

    [JSFunction]
    public static void printThis(string str) {
        Debug.Log(str);
    }
}
