﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

using Jurassic;

public class Coviz : MonoBehaviour {
    
    private string JsBundle = "./Assets/Scripts/JsScripts/main.txt";

    private Jurassic.ScriptEngine engine;

    // Use this for initialization
    void Start () {
        // maybe do this in Awake function?
        engine = new Jurassic.ScriptEngine();
        engine.SetGlobalValue("JsBridge", new JsBridge(engine));
        loadBundle(JsBundle);
        //Debug.Log(engine.Evaluate<string>("JsBridge.name + ' ' + JsBridge.version")); // example of exposing properties
        engine.Evaluate<string>("JsBridge.printThis('testing 123');"); // example of exposing functions to js
    }

    private void loadBundle(string jsBundlePath) {
        //var jsBundleAsset: TextAsset = Resources.Load("main", TextAsset);
        //jsBundleAsset.text;
        //runInJsEngine(File.ReadAllText(jsBundlePath));
        StreamReader reader = new StreamReader(jsBundlePath);
        runInJsEngine(reader.ReadToEnd());
        reader.Close();
    }

    public void runInJsEngine(string js) {
        engine.Evaluate(js);
    }
    
    // Update is called once per frame
    void Update () {
        
    }
}
